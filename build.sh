#!/usr/bin/env sh
if [ ! -f ./inject.js ]; then
	echo "window.confirm = function(message) { return true; };" >> ./inject.js
	echo "window.alert = function(message) {};" >> ./inject.js
	echo "window.prompt = function(message) { return null; };" >> ./inject.js
	echo "window.addEventListener('beforeunload', function(event) { event.stopPropagation(); window.onbeforeunload = null });" >> ./inject.js
	# Disable F5
	echo "window.addEventListener('keydown', function(event) { if (event.key === 'F5') { event.preventDefault(); event.stopPropagation() } });" >> ./inject.js
	# Disable CTRL + R
	echo "window.addEventListener('keydown', function(event) { if (event.ctrlKey && event.key === 'r') { event.preventDefault(); event.stopPropagation() } });" >> ./inject.js
fi

if [ ! -f ./icon.png ]; then
	wget -O ./icon.png https://www.jqbx.fm/images/jqbx.png
fi

nativefier "https://app.jqbx.fm" --internal-urls "^https://accounts.spotify.com|^https://app.jqbx.fm" --single-instance --inject ./inject.js

cp -f ./icon.png ./JQBX-linux-x64/resources/app/

# Cleanup
rm -f ./inject.js

