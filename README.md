This is a simple utility for building a JQBX desktop app. Tested on Ubuntu. Assumes you have `npm` and `nativefier` available in your PATH. Bring your own `.desktop` file.
